const projectDetail = {
    uuid: "db62b98b-24a9-4571-b001-432adb348b5a",
    subjectNumber: "200300",
    subjectName: "SV",
    projectUuid: "ebcc9df8-18bf-40f1-8ea7-1529294542a3",
    visitUuid: null,
    appointmentDate: null,
    lastEditBy: null,
    createdAt: "2023-08-15T04:52:21.000Z",
    updatedAt: "2023-08-15T04:52:21.000Z",
    activityUuid: null,
    project: {
        uuid: "ebcc9df8-18bf-40f1-8ea7-1529294542a3",
        projectName: "DW_DWP16001305",
        namePI: "THANITHA",
        recNumber: "65-404-9-1",
        isDeleted: false,
        isActive: true,
        lastEditBy: "36c34ddc-687b-4eea-ac39-dd06f857df5a",
        finishDate: null,
        createdAt: "2023-03-02T02:51:33.000Z",
        updatedAt: "2023-03-02T02:52:11.000Z",
    },
    admin: null,
};

const visitDetail = {
    uuid: "6843f53d-ba64-4b16-bab4-c080538f7658",
    visitName: "Run in visit",
    projectUuid: "ebcc9df8-18bf-40f1-8ea7-1529294542a3",
    isDeleted: false,
    lastEditBy: null,
    createdAt: "2023-03-02T08:53:31.000Z",
    updatedAt: "2023-03-02T08:53:31.000Z",
    project: {
        uuid: "ebcc9df8-18bf-40f1-8ea7-1529294542a3",
        projectName: "DW_DWP16001305",
        namePI: "THANITHA",
        recNumber: "65-404-9-1",
        isDeleted: false,
        isActive: true,
        lastEditBy: "36c34ddc-687b-4eea-ac39-dd06f857df5a",
        finishDate: null,
        createdAt: "2023-03-02T02:51:33.000Z",
        updatedAt: "2023-03-02T02:52:11.000Z",
    },
};

module.exports = { visitDetail, projectDetail };
