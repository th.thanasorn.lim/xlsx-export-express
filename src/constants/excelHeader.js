const activityHeaderKey = {
    activityName: "Activity Name",
    activityCost: "Activity Cost",
    overhead: "Overhead",
    done: "Done",
};

const projectInfoTitle = {
    projectName: "Project Name",
    namePi: "Name PI",
    visitName: "Visit Name",
    appointment: "Appointment",
    subjectName: "Subject Name",
    subjectNumber: "Subject Number",
};

const activityHeader = [
    activityHeaderKey.activityName,
    activityHeaderKey.activityCost,
    activityHeaderKey.overhead,
    activityHeaderKey.done,
];

module.exports = { activityHeader, projectInfoTitle };
