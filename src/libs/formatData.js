const moment = require("moment");
const { projectInfoTitle } = require("../constants/excelHeader");

function getActivityData(activityInfo) {
    let activityCost = 0;
    let overhead = 0;

    const rawExcelActivityData = activityInfo.map((activityData) => {
        const isDone = activityData.isDone ? "Yes" : "No";
        if (activityData.isDone) {
            console.log("Cal? ");
            activityCost += activityData.visitActivity.activityCost;
            overhead += activityData.visitActivity.overhead;
        }
        return [
            activityData.activity.activityName,
            activityData.visitActivity.activityCost,
            activityData.visitActivity.overhead,
            isDone,
        ];
    });
    const activityTotal = [
        "Total",
        activityCost,
        overhead,
        activityCost + overhead,
    ];
    rawExcelActivityData.push([]); // To add space between summary and activity
    rawExcelActivityData.push(activityTotal);

    return rawExcelActivityData;
}

function getProjectTitle(projectDetails, visitDetail) {
    const projectData = [
        projectInfoTitle.projectName,
        projectDetails.project.projectName,
        projectInfoTitle.namePi,
        projectDetails.project.namePI,
    ];
    const appointmentDate = projectDetails.appointmentDate
        ? moment(projectDetails.appointmentDate).format("dd/mm/yyyy")
        : "No appointments";
    const visitData = [
        projectInfoTitle.visitName,
        visitDetail.visitName,
        projectInfoTitle.appointment,
        appointmentDate,
    ];

    const subjectData = [
        projectInfoTitle.subjectName,
        projectDetails.subjectName,
        projectInfoTitle.subjectNumber,
        projectDetails.subjectNumber,
    ];
    return [projectData, visitData, subjectData];
}

module.exports = {
    getActivityData,
    getProjectTitle,
};
