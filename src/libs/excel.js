const { utils, writeFile } = require("xlsx-js-style");
const tmp = require("tmp");
const { getActivityData, getProjectTitle } = require("./formatData");

const getActivityExcelReport = (projInfo, visitDetail, activityData) => {
    const rawActivityData = getActivityData(activityData);
    const rawProjectData = getProjectTitle(projInfo, visitDetail);
    const activityHeader = [
        "Activity Name",
        "Activity Cost",
        "Overhead",
        "Done",
    ];
    const dataToGenerate = [
        ...rawProjectData,
        [],
        activityHeader,
        ...rawActivityData,
    ];
    const xlsxFile = generateExcel(dataToGenerate);
    return xlsxFile;
};

const generateExcel = (dataToGenerate) => {
    const tmpFile = tmp.fileSync({ postfix: ".xlsx" });
    const workbook = utils.book_new();
    const Condition = {
        Yes: "Yes",
        No: "No",
    };
    const defaultStyle = {
        font: {
            sz: 14, // Font size in points
        },
        alignment: { horizontal: "right" },
        // Add more style properties as needed
    };
    const labelStyles = {
        font: {
            bold: true,
            sz: defaultStyle.font.sz,
            // color: { rgb: "FF0000" }
        }, // Example: bold and red font
        alignment: { horizontal: "center" }, // Example: center align text
        fill: { fgColor: { rgb: "D9D9D9" } }, // Example: yellow background color
        // Add more styling properties as needed
    };

    const worksheet = utils.aoa_to_sheet(dataToGenerate);

    const columnBaseStyle = [];

    // [Debug] dataToGenerate[0] is highest columns
    const maxCol = dataToGenerate[0].length;
    const maxRow = dataToGenerate.length;
    const COLUMN_A_INDEX = 0;
    const COLUMN_C_INDEX = 2;
    const COLUMN_D_INDEX = 3;
    const PROJECT_INFO_ROW = 3;
    const ACTIVITY_LABEL_ROW = 4;

    for (let col = 0; col < maxCol; col++) {
        let maxContentWidth = 0;

        for (let row = 0; row < maxRow; row++) {
            // Get cell address eg. A1, A2, A3
            const cellAddress = utils.encode_cell({
                r: row,
                c: col,
            });
            const cellBody = worksheet[cellAddress];

            if (!cellBody) continue;

            // Convert number to type number and add commas separator
            if (!isNaN(Number(cellBody.v))) {
                cellBody.v = Number(cellBody.v);
                cellBody.z = "#,##0.00";
            }

            // Handle on Project Info
            if (
                row < PROJECT_INFO_ROW &&
                [COLUMN_A_INDEX, COLUMN_C_INDEX].includes(col)
            ) {
                // Do Project Label style
                if (col === COLUMN_A_INDEX) {
                    cellBody.s = labelStyles;
                }
                if (col === COLUMN_C_INDEX) {
                    cellBody.s = labelStyles;
                }
            }

            // Add style to activity header
            else if (row === ACTIVITY_LABEL_ROW) {
                // Handle activity label style
                cellBody.s = labelStyles;
            }

            // Add style to activity label
            else if (row > ACTIVITY_LABEL_ROW && col === COLUMN_A_INDEX) {
                // Handle activity style
                cellBody.s = labelStyles;
            }

            // Add style to activity [Done] on condition yes or no
            else if (
                row > ACTIVITY_LABEL_ROW &&
                col === COLUMN_D_INDEX &&
                cellBody.v === Condition.No
            ) {
                // Handle activity yes-no style
                cellBody.s = {
                    fill: {
                        fgColor: { rgb: "F08080" },
                    },
                    font: {
                        sz: 14,
                    },
                    alignment: { horizontal: "right" },
                };
            }
            // Add style to last rows (Total row)
            else if (row === maxRow - 1) {
                // Handle Total style
                cellBody.s = labelStyles;
            } else {
                if (!cellBody?.v.toString()) continue;
                if (cellBody?.s) continue;
                cellBody.s = defaultStyle;
            }
            console.log(maxRow);

            const cellValue = cellBody ? cellBody.v : "";
            const contentWidth = cellValue.toString().length;

            if (contentWidth > maxContentWidth) {
                maxContentWidth = contentWidth;
            }
        }
        // Convert the content width to a width value (adjust as needed)
        const columnWidth = maxContentWidth + 10; // Add extra padding (adjust as needed)

        // Store the column width for setting later
        columnBaseStyle.push({
            wch: columnWidth,
        });
    }

    worksheet["!cols"] = columnBaseStyle;
    utils.book_append_sheet(workbook, worksheet, "Sheet1");
    writeFile(workbook, tmpFile.name);
    return tmpFile;
};

module.exports = { generateExcel, getActivityExcelReport };
