const express = require("express");
const { getActivityExcelReport } = require("./src/libs/excel");
const { activityInfo } = require("./src/data/activityInfo");
const { projectDetail, visitDetail } = require("./src/data/visitDetail");

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (_, res) => {
    return res.status(200).send({ message: "Hello world!" });
});

app.get("/excel", (_, res) => {
    const reportFile = getActivityExcelReport(
        projectDetail,
        visitDetail,
        activityInfo
    );

    return res.download(reportFile.name, "jsonData.xlsx", (err) => {
        reportFile.removeCallback();

        if (err) {
            console.error("Error sending the file:", err);
        }
    });
});

app.listen(port, () => console.log("Server is running on port ", port));
