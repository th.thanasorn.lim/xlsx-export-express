# XLSX-EXPORT-EXPRESS

## Required Dependencies

- **moment**
- **xlsx**
- **tmp**
- **xlsx-js-style**

## Getting Started

To run the example, follow these steps:

1. Clone this project.
2. Run the following command:

   ```shell
   node index.js